"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _passportLocalMongoose = require("passport-local-mongoose");

var _passportLocalMongoose2 = _interopRequireDefault(_passportLocalMongoose);

var _config = require("../config");

var _config2 = _interopRequireDefault(_config);

var _account = require("../models/account");

var _account2 = _interopRequireDefault(_account);

var _authMiddleware = require("../middleware/authMiddleware");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var app = (0, _express.Router)();

  //api calls added here
  app.post("/register", function (req, res) {
    _account2.default.register(new _account2.default({ username: req.body.email }), req.body.password, function (err, account) {
      if (err) {
        res.send(err);
      }

      _passport2.default.authenticate("local", { session: false })(req, res, function () {
        res.status(200).send("Successfully created new account!");
      });
    });
  });

  //login
  app.post("/login", _passport2.default.authenticate("local", {
    session: false,
    scope: []
  }), _authMiddleware.generateAccessToken, _authMiddleware.respond);

  //logout
  app.get("/logout", _authMiddleware.authenticate, function (req, res) {
    res.logout();
    res.status(200).send("Successfully logged out!");
  });

  app.get("/me", _authMiddleware.authenticate, function (req, res) {
    res.status(200).json(req.user);
  });

  return app;
};
//# sourceMappingURL=accounts.js.map