"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _foodtruck = require("../models/foodtruck");

var _foodtruck2 = _interopRequireDefault(_foodtruck);

var _review = require("../models/review");

var _review2 = _interopRequireDefault(_review);

var _authMiddleware = require("../middleware/authMiddleware");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//BLAH 6
exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var app = (0, _express.Router)();

  //add data
  app.post("/add", _authMiddleware.authenticate, function (req, res) {
    var newFoodtruck = new _foodtruck2.default();
    newFoodtruck.name = req.body.name;
    newFoodtruck.cuisine = req.body.cuisine;
    newFoodtruck.avgCost = req.body.avgCost;
    newFoodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
    newFoodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

    newFoodtruck.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.send("Foodtruck saved!");
    });
  });

  //retrieve data
  app.get("/", function (req, res) {
    _foodtruck2.default.find({}, function (err, results) {
      //save() is error-first
      if (err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  app.get("/id/:id", function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, results) {
      if (err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  app.get("/name/:name", function (req, res) {
    _foodtruck2.default.find({ name: req.params.name }, function (err, results) {
      if (err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  app.get("/cuisine/:cuisine", function (req, res) {
    _foodtruck2.default.find({ cuisine: req.params.cuisine }, function (err, results) {
      if (err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  //update data
  app.put("/:id", _authMiddleware.authenticate, function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      foodtruck.name = req.body.name;
      foodtruck.cuisine = req.body.cuisine;
      foodtruck.avgCost = req.body.avgCost;
      foodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
      foodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

      foodtruck.save(function (err) {
        if (err) {
          res.send(err);
        }
        res.send("Foodtruck updated!");
      });
    });
  });

  //delete data
  app.delete("/:id", _authMiddleware.authenticate, function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.status(500).send(err);
        return;
      }
      if (foodtruck === null) {
        res.status(404).send("Foodtruck not found");
        return;
      }
      _foodtruck2.default.remove({ _id: req.params.id }, function (err, foodtruck) {
        if (err) {
          res.status(500).send(err);
          return;
        }
        _review2.default.remove({ forFoodtruck: req.params.id }, function (err, review) {
          if (err) {
            res.send(err);
          }
          res.send("Foodtruck and related reviews deleted!");
        });
      });
    });
  });

  //add reviews
  app.post("/reviews/add/:id", _authMiddleware.authenticate, function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      var newReview = new _review2.default();
      newReview.title = req.body.title;
      newReview.text = req.body.text;
      newReview.owner = req.body.owner;
      newReview.forFoodtruck = foodtruck._id;

      newReview.save(function (err) {
        if (err) {
          res.send(err);
        }
        foodtruck.reviews.push(newReview);
        foodtruck.save(function (err) {
          if (err) {
            res.send(err);
          }
        });
        res.send("Review saved!");
      });
    });
  });

  //get reviews
  app.get("/reviews/:id", function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      res.send(foodtruck.reviews);
    }).populate("reviews");
  });

  return app;
};
//# sourceMappingURL=foodtruck.js.map