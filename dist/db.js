"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (callback) {
  _mongoose2.default.Promise = global.promise;
  var dbConnection = _mongoose2.default.connect(_config2.default.mongoURL);
  callback(dbConnection);
};
//# sourceMappingURL=db.js.map