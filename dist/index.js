"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _routes = require("./routes");

var _routes2 = _interopRequireDefault(_routes);

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocalStrategy = require("passport-local").Strategy;

var Account = require("./models/account");

var app = (0, _express2.default)();
app.server = _http2.default.createServer(app);

app.use(_bodyParser2.default.urlencoded({ extended: true }));
app.use(_bodyParser2.default.json({
  limit: _config2.default.bodyLimit //BLAH 3
}));

app.use(_passport2.default.initialize());
_passport2.default.use(new LocalStrategy({
  usernameField: "email",
  passwordField: "password"
}, Account.authenticate()));
_passport2.default.serializeUser(Account.serializeUser());
_passport2.default.deserializeUser(Account.deserializeUser());

app.use("/api/v1", _routes2.default);

app.server.listen(_config2.default.port);
console.log("Listening on " + app.server.address().port);

exports.default = app; //BLAH 2


/*
This index.js is using –
1) the http library, which is part of node's NATIVE modules. when we import it using ES6, babel transpiles it
    using it's presets (which are essentially modules that help with transpilation) into older versions of JS
    (that the browser can understand). this leads us to the usage of "require". the "require" function first
    checks for NATIVE modules, then checks for any Node Modules we may have added using NPM, then lastly
    (if you perchance missed out on ./ for specifying a local module) checks for local. in this case,
    require catches "http" as part of the native module.

    we can go much into depth here. http itself is a library that allows us to manipulate the OS'es capability
    to create http requests and use ports to send and recieve data. with the help of the "libuv" library,
    which is part of Node's C++ core, we are able to achieve asychronous, concurrent behavior that allows
    us to deal with these requests and continue running our original ^ code at the same time.

    anyway.

  2) express - kind of like a wrapper around the http functionalities of node. helps create and manage servers.

  3) mongoose - provides structure and "schemas" for the noSQL mongo database. also allows us to communication with
    the db using methods like save(), and search for documents using find() and so on.

  4) body parser - allows us to read the BODY data of a POST request.
    the way this is done is that, body parser actually reads the body of a standard HTTP "POST" request,
    and attaches whatever it finds to the "req" OBJECT using a (new) property called "body" – creating
    "req.body". The rest of the properties found in the body (if JSON) can be accessed similarly, e.g.
    "req.body.name". This is all done by body parser.

  5) Routes – kind of like a higher level routing file; directs you to a specific CONTROLLER based on the
    url provided. e.g. all "/restaurant" requests are further redirected to the RESTAURANT CONTROLLER.

  6) Config – just contains a JSON object with key/value pairs that hold config info like mongourl and what
    port we want to work on.

  Now, calling the express constructor RETURNS a function. Which is why "app" can be used as a parameter in
  http.createServer(). This is possible because JavaScript consists of FIRST-CLASS FUNCTIONS, in that like
  variables and objects, even functions can be passed around via parameters and returns.
  "let" is just an ES6 version of "var" – the alternative ES6 variable is "const" - for immutable variables.

  We've thought about the bodyParser config – I really think this is library-specific. You just have to know it.

  For port, we should use environmental variables.

  Right now, ALL routes are answering only to "localhost:3005/v1/{x}", where {x} = endpoint URL.
  This is actually a good way to separate between versions.

  Side tidbit - logging here is using template literals, which is an ES6 addition.
  This way we don't have to use string concatenation.

  Ok I'll stop here.

*/
//# sourceMappingURL=index.js.map