"use strict";

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _review = require("./review");

var _review2 = _interopRequireDefault(_review);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
foodtruckChef, foodtruckHours, foodtruckDays , foodtruckRating (Double?), foodtruckMenu (needs it's own schema),
doesDeliver (Boolean), doesPickup (Boolean), website (String), phoneNumber (String/Phone), keywords [String],
isClaimed (Boolean)
rayaVisitedBy (do we really want to keep a list of people who've visited? what if it grows immensely?)

OH OH OH OH OH. GET FOODTRUCK DATA FROM EXTERNAL SOURCES.
GOOD DATA RESOURCES?

*/

var Schema = _mongoose2.default.Schema;
var foodtruckSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  cuisine: {
    type: String,
    required: true
  },
  avgCost: Number,
  geometry: {
    type: {
      type: String,
      default: "Point"
    },
    coordinates: {
      "lat": Number,
      "long": Number
    }
  },
  reviews: {
    type: [{
      type: Schema.Types.ObjectId,
      ref: "Review"
    }]
  }
});

module.exports = _mongoose2.default.model("Foodtruck", foodtruckSchema);
//# sourceMappingURL=foodtruck.js.map