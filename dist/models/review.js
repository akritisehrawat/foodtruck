"use strict";

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _foodtruck = require("./foodtruck");

var _foodtruck2 = _interopRequireDefault(_foodtruck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

/*
dateReviewed, rating, upvotes, downvotes, comments, isOwnerAnonymous
*/

var ReviewSchema = new Schema({
  title: {
    type: String,
    required: [true, "Review Title is required"]
  },
  text: {
    type: String
  },
  owner: String,
  forFoodtruck: {
    type: Schema.Types.ObjectId,
    ref: 'Foodtruck',
    required: true
  }
});

module.exports = _mongoose2.default.model("Review", ReviewSchema);
//# sourceMappingURL=review.js.map