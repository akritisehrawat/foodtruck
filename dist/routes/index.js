"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _config = require("../config");

var _config2 = _interopRequireDefault(_config);

var _middleware = require("../middleware");

var _middleware2 = _interopRequireDefault(_middleware);

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _foodtruck = require("../controllers/foodtruck");

var _foodtruck2 = _interopRequireDefault(_foodtruck);

var _accounts = require("../controllers/accounts");

var _accounts2 = _interopRequireDefault(_accounts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

(0, _db2.default)(function (db) {
  //BLAH 8
  app.use((0, _middleware2.default)({ config: _config2.default, db: db }));
  app.use("/foodtruck", (0, _foodtruck2.default)({ config: _config2.default, db: db }));
  app.use("/accounts", (0, _accounts2.default)({ config: _config2.default, db: db }));
});

exports.default = app;
//# sourceMappingURL=index.js.map