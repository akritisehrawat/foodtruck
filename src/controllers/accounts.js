import { Router } from "express";
import bodyParser from "body-parser";
import passport from "passport";
import passportLocalMongoose from "passport-local-mongoose";

import config from "../config";
import Account from "../models/account";

import { generateAccessToken, respond, authenticate } from "../middleware/authMiddleware";

export default ({config, db}) => {
    let app = Router();

    //api calls added here
    app.post("/register", (req, res) => {
      Account.register(new Account({username: req.body.email}), req.body.password, (err, account) => {
        if(err) {
          return res.send(err);
        }

        passport.authenticate("local", {session: false})(req, res, () => {
          res.status(200).send("Successfully created new account!");
        });
      });
    });

    //login
    app.post("/login", passport.authenticate(
      "local", {
        session: false,
        scope: []
    }), generateAccessToken, respond);



    //logout
    app.get("/logout", authenticate, (req, res) => {
      res.logout();
      res.status(200).send("Successfully logged out!");
    });

    app.get("/me", authenticate, (req, res) => {
      res.status(200).json(req.user);
    });

    return app;
}
