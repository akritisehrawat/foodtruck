import { Router } from "express"; //BLAH 6
import mongoose from "mongoose";
import Foodtruck from "../models/foodtruck";
import Review from "../models/review";

import { authenticate } from "../middleware/authMiddleware";

export default ({ config, db }) => {
  let app = Router();

  //add data
  app.post("/add", authenticate, (req, res) => {
    let newFoodtruck = new Foodtruck();
    newFoodtruck.name = req.body.name;
    newFoodtruck.cuisine = req.body.cuisine;
    newFoodtruck.avgCost = req.body.avgCost;
    newFoodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
    newFoodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

    newFoodtruck.save(err => {
      if(err) {
        res.send(err);
      }
      res.send("Foodtruck saved!");
    });
  });


  //retrieve data
  app.get("/", (req, res) => {
    Foodtruck.find({}, (err, results) => { //save() is error-first
      if(err) {
        res.send(err);
      }
      res.send(results);
    })
    .populate("reviews");
  });

  app.get("/id/:id", (req, res) => {
    Foodtruck.findById(req.params.id, (err, results) => {
      if(err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  app.get("/name/:name", (req, res) => {
    Foodtruck.find({name: req.params.name}, (err, results) => {
      if(err) {
        res.send(err);
      }
      res.send(results);
    }).populate("reviews");
  });

  app.get("/cuisine/:cuisine", (req, res) => {
    Foodtruck.find({cuisine: req.params.cuisine }, (err, results) => {
        if(err) {
          res.send(err);
        }
        res.send(results);
    }).populate("reviews");
  });


  //update data
  app.put("/:id", authenticate, (req, res) => {
    Foodtruck.findById(req.params.id, (err, foodtruck) => {
        if(err) {
          res.send(err);
        }
        foodtruck.name = req.body.name;
        foodtruck.cuisine = req.body.cuisine;
        foodtruck.avgCost = req.body.avgCost;
        foodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
        foodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

        foodtruck.save(err => {
          if(err) {
            res.send(err);
          }
          res.send("Foodtruck updated!");
        });
    });
  });


  //delete data
  app.delete("/:id", authenticate, (req, res) => {
    Foodtruck.findById(req.params.id, (err, foodtruck) => {
      if(err) {
        res.status(500).send(err);
        return;
      }
      if(foodtruck === null) {
        res.status(404).send("Foodtruck not found");
        return;
      }
      Foodtruck.remove({_id: req.params.id}, (err, foodtruck) => {
        if(err) {
          res.status(500).send(err);
          return;
        }
        Review.remove({forFoodtruck: req.params.id}, (err, review) => {
          if(err) {
            res.send(err);
          }
          res.send("Foodtruck and related reviews deleted!");
        });
      });
    });
  });


  //add reviews
  app.post("/reviews/add/:id", authenticate, (req, res) => {
    Foodtruck.findById(req.params.id, (err, foodtruck) => {
      if(err) {
        res.send(err);
      }
      let newReview = new Review();
      newReview.title = req.body.title;
      newReview.text = req.body.text;
      newReview.owner = req.body.owner;
      newReview.forFoodtruck = foodtruck._id;

      newReview.save(err => {
        if(err) {
          res.send(err);
        }
        foodtruck.reviews.push(newReview);
        foodtruck.save(err => {
          if(err) {
            res.send(err);
          }
        });
        res.send("Review saved!");
      });
    });
  });

  //get reviews
  app.get("/reviews/:id", (req, res) => {
    Foodtruck.findById(req.params.id, (err, foodtruck) => {
      if(err) {
        res.send(err);
      }
      res.send(foodtruck.reviews);
    })
    .populate("reviews");
  });

  return app;
}
