import config from "./config";
import express from "express";
import mongoose from "mongoose";

export default callback => {
  let dbConnection = mongoose.connect(config.mongoURL);
  callback(dbConnection);
}
