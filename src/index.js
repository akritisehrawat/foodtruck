import http from "http";
import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import passport from "passport";
const LocalStrategy = require("passport-local").Strategy;

import routes from "./routes";
import config from "./config";
let Account = require("./models/account");

let app = express();
app.server = http.createServer(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({
  limit: config.bodyLimit //BLAH 3
}));

app.use(passport.initialize());
passport.use(new LocalStrategy({
  usernameField: "email",
  passwordField: "password"
},
Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

app.use("/api/v1", routes);

app.server.listen(config.port);
console.log(`Listening on ${app.server.address().port}`);

export default app; //BLAH 2


/*
This index.js is using –
1) the http library, which is part of node's NATIVE modules. when we import it using ES6, babel transpiles it
    using it's presets (which are essentially modules that help with transpilation) into older versions of JS
    (that the browser can understand). this leads us to the usage of "require". the "require" function first
    checks for NATIVE modules, then checks for any Node Modules we may have added using NPM, then lastly
    (if you perchance missed out on ./ for specifying a local module) checks for local. in this case,
    require catches "http" as part of the native module.

    we can go much into depth here. http itself is a library that allows us to manipulate the OS'es capability
    to create http requests and use ports to send and recieve data. with the help of the "libuv" library,
    which is part of Node's C++ core, we are able to achieve asychronous, concurrent behavior that allows
    us to deal with these requests and continue running our original ^ code at the same time.

    anyway.

  2) express - kind of like a wrapper around the http functionalities of node. helps create and manage servers.

  3) mongoose - provides structure and "schemas" for the noSQL mongo database. also allows us to communication with
    the db using methods like save(), and search for documents using find() and so on.

  4) body parser - allows us to read the BODY data of a POST request.
    the way this is done is that, body parser actually reads the body of a standard HTTP "POST" request,
    and attaches whatever it finds to the "req" OBJECT using a (new) property called "body" – creating
    "req.body". The rest of the properties found in the body (if JSON) can be accessed similarly, e.g.
    "req.body.name". This is all done by body parser.

  5) Routes – kind of like a higher level routing file; directs you to a specific CONTROLLER based on the
    url provided. e.g. all "/restaurant" requests are further redirected to the RESTAURANT CONTROLLER.

  6) Config – just contains a JSON object with key/value pairs that hold config info like mongourl and what
    port we want to work on.

  Now, calling the express constructor RETURNS a function. Which is why "app" can be used as a parameter in
  http.createServer(). This is possible because JavaScript consists of FIRST-CLASS FUNCTIONS, in that like
  variables and objects, even functions can be passed around via parameters and returns.
  "let" is just an ES6 version of "var" – the alternative ES6 variable is "const" - for immutable variables.

  We've thought about the bodyParser config – I really think this is library-specific. You just have to know it.

  For port, we should use environmental variables.

  Right now, ALL routes are answering only to "localhost:3005/v1/{x}", where {x} = endpoint URL.
  This is actually a good way to separate between versions.

  Side tidbit - logging here is using template literals, which is an ES6 addition.
  This way we don't have to use string concatenation.

  Ok I'll stop here.

*/
