import mongoose from "mongoose";
import Review from "./review";

/*
for v2 - brainstorm - foodtruckChef, foodtruckHours, foodtruckDays , foodtruckRating (Double?), foodtruckMenu (needs it's own schema),
doesDeliver (Boolean), doesPickup (Boolean), website (String), phoneNumber (String/Phone), keywords [String],
isClaimed (Boolean)
rayaVisitedBy (do we really want to keep a list of people who've visited? what if it grows immensely?)

OH OH OH OH OH. GET FOODTRUCK DATA FROM EXTERNAL SOURCES.
GOOD DATA RESOURCES?

*/

let Schema = mongoose.Schema;
let foodtruckSchema = new Schema({
    name: {
      type: String,
      required: true
    },
    cuisine: {
      type: String,
      required: true
    },
    avgCost: Number,
    geometry: {
      type: {
        type: String,
        default: "Point"
      },
      coordinates: {
        "lat": Number,
        "long": Number
      }
    },
    reviews: {
      type: [{
        type: Schema.Types.ObjectId,
        ref: "Review"
      }]
    }
});

module.exports = mongoose.model("Foodtruck", foodtruckSchema);
