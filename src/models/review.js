import mongoose from "mongoose";
import Foodtruck from "./foodtruck";

let Schema = mongoose.Schema;

/*
for v2 - dateReviewed, rating, upvotes, downvotes, comments, isOwnerAnonymous
*/

let ReviewSchema = new Schema({
  title: {
    type: String,
    required: [true, "Review Title is required"]
  },
  text: {
    type: String
  },
  owner: String,
  forFoodtruck: {
    type: Schema.Types.ObjectId,
    ref: 'Foodtruck',
    required: true
  }
});

module.exports = mongoose.model("Review", ReviewSchema);
