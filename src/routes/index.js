import express from "express";
import config from "../config";
import middleware from "../middleware";
import initializeDB from "../db";
import foodtruck from "../controllers/foodtruck";
import accounts from "../controllers/accounts";

let app = express();

initializeDB(db => { //BLAH 8
  app.use(middleware({config, db}));
  app.use("/foodtruck", foodtruck({config, db}));
  app.use("/accounts", accounts({config, db}))
});

export default app;
